# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-03-04 09:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gmail_scraper', '0006_email_email_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='email',
            name='email_date',
            field=models.DateTimeField(blank=True, default=None, null=True),
        ),
    ]
