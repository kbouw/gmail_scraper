# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-03-04 10:03
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gmail_scraper', '0010_auto_20160304_0952'),
    ]

    operations = [
        migrations.RenameField(
            model_name='email',
            old_name='sent_form',
            new_name='sent_from',
        ),
    ]
