# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-03-04 06:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gmail_scraper', '0002_email_email_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='email',
            name='sales_lead',
            field=models.BooleanField(default=False),
        ),
    ]
