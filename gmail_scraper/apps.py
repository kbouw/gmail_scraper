from __future__ import unicode_literals

from django.apps import AppConfig


class GmailScraperConfig(AppConfig):
    name = 'gmail_scraper'
