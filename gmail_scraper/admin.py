from django.contrib import admin

from .models import Email


# class ChoiceInline(admin.TabularInline):
#     model = Email
#     extra = 3

def make_as_sale_lead(modeladmin, request, queryset):
    queryset.update(sales_lead=True)
make_as_sale_lead.short_description = "Mark as sale lead"

def make_as_not_sale_lead(modeladmin, request, queryset):
    queryset.update(sales_lead=False)
make_as_not_sale_lead.short_description = "Mark as not sale lead"

class EmailAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['email_id']}),
        (None,               {'fields': ['email_date']}),

        (None,               {'fields': ['delivered_to']}),
        (None,               {'fields': ['sent_from']}),
        (None,               {'fields': ['subject_line']}),
        (None,               {'fields': ['message_text']}),
        (None,               {'fields': ['sales_lead']}),

        ('Date information', {'fields': ['email_date'], 'classes': ['collapse']}),
    ]
    # inlines = [ChoiceInline]
    list_display = ("email_id","delivered_to",  "sent_from","subject_line","short_message", "email_date","sales_lead")
    list_filter = ['email_date']
    search_fields = ['message_text']
    actions = [make_as_sale_lead, make_as_not_sale_lead]

# admin.site.register(Email)
admin.site.register(Email, EmailAdmin)

