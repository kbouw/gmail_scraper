# -*- coding: utf-8 -*-
import sys  

reload(sys)  
sys.setdefaultencoding('utf8')


import imaplib, re
import pprint
import email
import email.header
from time import sleep
from time import sleep

import insert_email
import datetime

user = 'thinh.ly@inex.sg'
pwd = '@LEE1992'

class pygmail(object):
    def __init__(self):
        self.IMAP_SERVER='imap.gmail.com'
        self.IMAP_PORT=993
        self.M = None
        self.response = None
        self.mailboxes = []

    def login(self, username, password):
        self.M = imaplib.IMAP4_SSL(self.IMAP_SERVER, self.IMAP_PORT)
        rc, self.response = self.M.login(username, password)
        return rc

    def get_mailboxes(self):
        rc, self.response = self.M.list()
        for item in self.response:
            self.mailboxes.append(item.split()[-1])
        return rc, self.response

    def get_mail_count(self, folder='Inbox'):
        rc, self.response = self.M.select(folder)
        return self.response[0]

    def get_unread_count(self, folder='Inbox'):
        rc, self.response = self.M.status(folder, "(UNSEEN)")
        unreadCount = re.search("UNSEEN (\d+)", self.response[0]).group(1)
        return unreadCount

    def get_imap_quota(self):
        quotaStr = self.M.getquotaroot("Inbox")[1][1][0]
        r = re.compile('\d+').findall(quotaStr)
        if r == []:
            r.append(0)
            r.append(0)
        return float(r[1])/1024, float(r[0])/1024

    def get_mails_from(self, uid, folder='Inbox'):
        status, count = self.M.select(folder, readonly=1)
        status, response = self.M.search(None, 'FROM', uid)
        email_ids = [e_id for e_id in response[0].split()]
        return email_ids

    def get_mail_from_id(self, id):
        status, response = self.M.fetch(id, '(body[header.fields (subject)])')
        return response

    def rename_mailbox(self, oldmailbox, newmailbox):
        rc, self.response = self.M.rename(oldmailbox, newmailbox)
        return rc

    def create_mailbox(self, mailbox):
        rc, self.response = self.M.create(mailbox)
        return rc

    def delete_mailbox(self, mailbox):
        rc, self.response = self.M.delete(mailbox)
        return rc

    def logout(self):
        self.M.logout()

    def get_all_emails(self, folder='Inbox'):
        rc, self.response = self.M.select(folder)

        rv, data = self.M.search(None, "ALL")

        for num in data[0].split():
            print num

            rv, data = self.M.fetch(num, '(RFC822)')    
            msg = email.message_from_string(data[0][1])
            body = ""

            if msg.is_multipart():
                for part in msg.walk():
                    ctype = part.get_content_type()
                    cdispo = str(part.get('Content-Disposition'))

                    # skip any text/plain (txt) attachments
                    if ctype == 'text/plain' and 'attachment' not in cdispo:
                        body = part.get_payload(decode=True)  # decode
                        break
            # not multipart - i.e. plain text, no attachments, keeping fingers crossed
            else:
                body = msg.get_payload(decode=True)

            print body

            date_str=msg.get('date')

            date=None
            if date_str:
                date_tuple=email.utils.parsedate_tz(date_str)
                if date_tuple:
                    date=datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
            if date:
                print date
                print type(date)


            email_object={"delivered_to":None, "sent_from":None,"subject_line":None,"message_text":None, "email_id":None, "email_date":None}
            email_object["email_id"] = num
            email_object["delivered_to"]=msg['To']
            email_object["sent_from"]=msg['From']
            email_object["subject_line"]=msg['Subject']
            email_object["message_text"]=body
            email_object["email_date"] = date      

            pprint.pprint(email_object)
            print email_object["message_text"]

            try:
                insert_email.insert_email_to_db(delivered_to=email_object["delivered_to"], sent_from=email_object["sent_from"],subject_line=email_object["subject_line"],message_text=email_object["message_text"], email_id=email_object["email_id"],email_date=email_object["email_date"] )
            
            except Exception as e:
                print e

            # insert_email.insert_email_to_db(delivered_to=email_object["delivered_to"], sent_from=email_object["sent_from"],subject_line=email_object["subject_line"],message_text=email_object["message_text"], email_id=email_object["email_id"],email_date=email_object["email_date"] )
            
            print "===--ending--==="


            # sleep(1000)



def get_all_inbox_emails():

    gm = pygmail()
    gm.login(user, pwd)


    print gm.get_all_emails()

# print gm.get_mail_count()
if __name__ == '__main__':
    print "na"
    get_all_inbox_emails()
