from __future__ import unicode_literals

from django.db import models
from django.template.defaultfilters import truncatechars  # or truncatewords


class Email(models.Model):
    delivered_to = models.CharField(max_length=200)
    sent_from = models.CharField(max_length=200)
    subject_line = models.CharField(max_length=200)
    
    # to get unlimited length for text field
    message_text = models.TextField()
    email_id = models.IntegerField(default=0, unique=True)
    sales_lead = models.BooleanField(default=False)

    email_date = models.DateTimeField(default=None, null=True)

    @property
    def short_message(self):
        return truncatechars(self.message_text, 200)
