import os
import sys
sys.path.insert(1,os.path.dirname(os.path.abspath(os.path.dirname(__name__))))


#Django settings
from django.conf import settings
settings.configure(DEBUG=False,
   DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'gmail_scraper',
        'USER': 'pguser',
        'PASSWORD': 'password',
        'HOST': '',
        'PORT': '5432',
    }
},
    INSTALLED_APPS = ('gmail_scraper.apps.GmailScraperConfig',)
)


import django
django.setup()

from gmail_scraper.models import *


def insert_email_to_db(delivered_to, sent_from,subject_line,message_text, email_id,email_date):
	email = Email(delivered_to=delivered_to, sent_from=sent_from,subject_line=subject_line,message_text=message_text, email_id= email_id, email_date=email_date)
	status = email.save()
	print status
#Email.objects.all()[0].delivered_to


# # The above two lines could be written simply as:
# # from project.wsgi import *

# from xxx.models import import TransType,Trans
# TransType.objects.create()
# Trans.objects.create()


# Email._meta._get_fields()
